/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viagogo_test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 *this class generates seed data for the events.
 * @author Josh
 */
public class DataGenerator {
    
    public ArrayList<Event> createEvents(){
        ArrayList<Event> eventArrayList = new ArrayList<Event>();
        Integer count = 0;
        //creating random data using a seed to make sure the data is truly random.
        for(int i =0; i<30;i++){
            Event temp = new Event();
            Random rand = new Random(System.nanoTime());   
            Double tempPrice;
            tempPrice = (double)(Math.round(rand.nextDouble()*150));
            Integer tempX= rand.nextInt(20)-10;
            Integer tempY= rand.nextInt(20)-10;
            
            //adding one value to the arraylist if the size is equal to 0.
            if(eventArrayList.size()==0){
                    temp.setEventCode(i+1);
                    temp.setTicketPrice(tempPrice);
                    temp.setxCoord(tempX);
                    temp.setyCoord(tempY);
                    temp.setNumberOfTickets(rand.nextInt(200));
                    eventArrayList.add(temp);
                    
            }else{
                //making sure that there can only be one event in any one place.
                for(int j =0; j<count; j++){
                    if(tempX != eventArrayList.get(j).getxCoord() && tempY != eventArrayList.get(j).getyCoord()){
                        temp.setEventCode(i+1);
                        temp.setTicketPrice(tempPrice);
                        temp.setxCoord(tempX);
                        temp.setyCoord(tempY);
                        temp.setNumberOfTickets(rand.nextInt(200));
                        eventArrayList.add(temp);
                        
                    }else{

                     break;

                    }
                }
            }
            count++;
        }
        
        return eventArrayList;
    }
}

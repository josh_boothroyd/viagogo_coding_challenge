/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viagogo_test;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.RED;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 *
 * @author Josh
 */
public class MainFrameFx extends Application {
    public ArrayList<Event> events = new ArrayList<Event>();
    @Override
    public void start(Stage primaryStage) {
        setUpFrame(primaryStage);
        
        
    }
//

    public ArrayList<Event> calculateDistance(ArrayList<Event> list, Integer x, Integer y, ScatterChart sc){
            Integer distance1=1000;
            Integer distance2=1000;
            Integer distance3=1000;
            Integer distance4=1000;
            Integer distance5=1000;
            
            ArrayList<Event> nearestEvents = new ArrayList<Event>();
            Event event1 = new Event();
            Event event2 = new Event();
            Event event3 = new Event();
            Event event4 = new Event();
            Event event5 = new Event();
            
        for(int i =0; i< list.size(); i++){
            Integer eventX = list.get(i).getxCoord();
            Integer eventY = list.get(i).getyCoord();
            Integer tempDistance;
            //method for calculating the direction and distance to events.
            Integer tempX=x-eventX;
            Integer tempY=y-eventY;
            
            String directionX;
            String directionY; 
            String directions;
           
            
                 tempDistance=(Math.abs(tempX)+Math.abs(tempY));   
            if(tempX<0){
                directionX = "East  "+Math.abs(tempX);
            }else{
                directionX = "West "+Math.abs(tempX);
            }
            
            if(tempY<0){
                directionY = "North "+Math.abs(tempY);
            }else{
                directionY = "South "+Math.abs(tempY);
            }
            
            directions = directionX +" then " +directionY;
            //method to setting the top 5 events into their values and assigning them to a new ArrayList.
            if(tempDistance<distance1){
                distance1=tempDistance;
                event1.setDistance(distance1);
                event1.setEventCode(list.get(i).getEventCode());
                event1.setTicketPrice(list.get(i).getTicketPrice());
                event1.setxCoord(list.get(i).getxCoord());
                event1.setyCoord(list.get(i).getyCoord());
                event1.setNumberOfTickets(list.get(i).getNumberOfTickets());
                event1.setDirections(directions);
                
                
                
                        
            }else if(tempDistance<distance2){
                distance2=tempDistance;
                event2.setDistance(distance2);
                event2.setEventCode(list.get(i).getEventCode());
                event2.setTicketPrice(list.get(i).getTicketPrice());
                event2.setxCoord(list.get(i).getxCoord());
                event2.setyCoord(list.get(i).getyCoord());
                event2.setNumberOfTickets(list.get(i).getNumberOfTickets());
                event2.setDirections(directions);
                
                
            }else if(tempDistance<distance3){
                distance3=tempDistance;
                event3.setDistance(distance3);
                event3.setEventCode(list.get(i).getEventCode());
                event3.setTicketPrice(list.get(i).getTicketPrice());
                event3.setxCoord(list.get(i).getxCoord());
                event3.setyCoord(list.get(i).getyCoord());
                event3.setNumberOfTickets(list.get(i).getNumberOfTickets());
                event3.setDirections(directions);

            }
            else if(tempDistance<distance4){
                distance4=tempDistance;
                event4.setDistance(distance4);
                event4.setEventCode(list.get(i).getEventCode());
                event4.setTicketPrice(list.get(i).getTicketPrice());
                event4.setxCoord(list.get(i).getxCoord());
                event4.setyCoord(list.get(i).getyCoord());
                event4.setNumberOfTickets(list.get(i).getNumberOfTickets());
                event4.setDirections(directions);

            }
            else if(tempDistance<distance5){
                distance5=tempDistance;
                event5.setDistance(distance5);
                event5.setEventCode(list.get(i).getEventCode());
                event5.setTicketPrice(list.get(i).getTicketPrice());
                event5.setxCoord(list.get(i).getxCoord());
                event5.setyCoord(list.get(i).getyCoord());
                event5.setNumberOfTickets(list.get(i).getNumberOfTickets());
                event5.setDirections(directions);

            }
        
        }
        nearestEvents.add(event1);       
        nearestEvents.add(event2);
        nearestEvents.add(event3);
        nearestEvents.add(event4);
        nearestEvents.add(event5);
        

        return nearestEvents;
    }
    //method to set up the events dialog showing closest events
    public void dialogEvents(ArrayList<Event> events, ScatterChart sc){
        Dialog dialog = new Dialog();
       

        //set up the dialog full of  closest events.
        dialog.setTitle("Your nearest Events");
        dialog.setHeaderText("Events");
        
        
        ButtonType close = new ButtonType("Change Location",ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(close);
        
        
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        
        
        VBox lblBox = new VBox();
        Label event = new Label("*");
        Label code = new Label("Event Code: ");
        Label coordinates = new Label("Event Coordinates: ");
        Label cost = new Label("Price: ");
        Label distance = new Label("Distance from location: ");
        Label noOfTickets= new Label("Number of Tickets Left: ");
        Label directions = new Label("Directions:");
        lblBox.getChildren().addAll(event,code,coordinates, cost, distance,noOfTickets,directions);
        lblBox.setPadding(new Insets(0,0,10,10));
        lblBox.setMinWidth(120);
        
        VBox vbox1 = new VBox();
        Label event1 = new Label("Event 1");
        Label eventcode1 = new Label( events.get(0).getEventCode().toString());
        String Coords1 = events.get(0).getxCoord().toString()+"," + events.get(0).getyCoord().toString();
        Label coordlbl1= new Label(Coords1);
        Label eventCost1= new Label("$"+events.get(0).getTicketPrice().toString());
        Label eventDistance1= new Label(events.get(0).getDistance().toString());
        Label numbTickets1= new Label(events.get(0).getNumberOfTickets().toString());
        Label directions1 = new Label(events.get(0).getDirections());
        
        vbox1.getChildren().addAll(event1,eventcode1, coordlbl1, eventCost1,eventDistance1, numbTickets1,directions1);
        vbox1.setMinWidth(120);
        
        VBox vbox2 = new VBox();
        Label event2 = new Label("Event 2");
        Label eventcode2= new Label(events.get(1).getEventCode().toString());
        String Coords2 = events.get(1).getxCoord().toString()+"," + events.get(1).getyCoord().toString();
        Label coordlbl2= new Label(Coords2);
        Label eventCost2= new Label("$"+events.get(1).getTicketPrice().toString());
        Label eventDistance2= new Label(events.get(1).getDistance().toString());
        Label numbTickets2= new Label(events.get(1).getNumberOfTickets().toString());
        Label directions2 = new Label(events.get(1).getDirections());
        
        vbox2.getChildren().addAll(event2, eventcode2,coordlbl2, eventCost2, eventDistance2, numbTickets2, directions2);
        vbox2.setMinWidth(120);        
                
        VBox vbox3 = new VBox();
        Label event3 = new Label("Event 3");
        Label eventcode3= new Label(events.get(2).getEventCode().toString());
        String Coords3 = events.get(2).getxCoord().toString()+"," + events.get(2).getyCoord().toString();
        Label coordlbl3= new Label(Coords3);
        Label eventCost3= new Label("$"+events.get(2).getTicketPrice().toString());
        Label eventDistance3 = new Label(events.get(2).getDistance().toString());
        Label numbTickets3= new Label(events.get(2).getNumberOfTickets().toString());
        Label directions3 = new Label(events.get(2).getDirections());
        
        vbox3.getChildren().addAll(event3,eventcode3, coordlbl3, eventCost3, eventDistance3,numbTickets3,directions3);
        vbox3.setMinWidth(120);
        
        VBox vbox4 = new VBox();
        Label event4 = new Label("Event 4");
        Label eventcode4= new Label(events.get(3).getEventCode().toString());
        String Coords4 = events.get(3).getxCoord().toString()+"," + events.get(3).getyCoord().toString();
        Label coordlbl4= new Label(Coords4);
        Label eventCost4= new Label("$"+events.get(3).getTicketPrice().toString());
        Label eventDistance4 = new Label(events.get(3).getDistance().toString());
        Label numbTickets4= new Label(events.get(3).getNumberOfTickets().toString());
        Label directions4 = new Label(events.get(3).getDirections());

        vbox4.getChildren().addAll(event4,eventcode4, coordlbl4, eventCost4, eventDistance4, numbTickets4,directions4);
        vbox4.setMinWidth(120);
        
        VBox vbox5 = new VBox();
        Label event5 = new Label("Event 5");
        Label eventcode5= new Label(events.get(4).getEventCode().toString());
        String Coords5 = events.get(4).getxCoord().toString()+"," + events.get(4).getyCoord().toString();
        Label coordlbl5= new Label(Coords5);
        Label eventCost5= new Label("$"+events.get(4).getTicketPrice().toString());
        Label eventDistance5 = new Label(events.get(4).getDistance().toString());
        Label numbTickets5= new Label(events.get(4).getNumberOfTickets().toString());
        Label directions5 = new Label(events.get(4).getDirections());
        
        vbox5.getChildren().addAll(event5,eventcode5, coordlbl5, eventCost5, eventDistance5,numbTickets5,directions5);
        vbox5.setMinWidth(120);
        
        HBox container1 = new HBox();
        container1.getChildren().addAll(lblBox, vbox1, vbox2, vbox3, vbox4, vbox5);
        grid.getChildren().addAll(container1);
        
        grid.setHgap(50);
        grid.setVgap(50);
        grid.setPadding(new Insets(20, 150, 20, 20));
        dialog.getDialogPane().setMinSize(400, 400);
        dialog.getDialogPane().setMinWidth(800);
        
        dialog.getDialogPane().setContent(grid);
        dialog.showAndWait();
    }
    //method to set up the main window of the application.
    public void setUpFrame(Stage primaryStage){
        //regex pattern used to santise the user input.
        Pattern pattern =  Pattern.compile("^-?[0-9]\\d?");
        DataGenerator generator = new DataGenerator();
        generator.createEvents();
        
        primaryStage.setTitle("Viagogo Map of Events");
         
        events = generator.createEvents();
        
        //setting the limits of the map.
        final NumberAxis xAxis = new NumberAxis(-10, 10, 1);
        final NumberAxis yAxis = new NumberAxis(-10, 10, 1);    
        
        final ScatterChart<Number,Number> sc = new
            ScatterChart<Number,Number>(xAxis,yAxis);
       
       
        sc.setTitle("Viagogo Map of Events");
        
        XYChart.Series series = new XYChart.Series();
        
        series.setName("Event locations");
        //adding the seed data to the new series.
        for(int i = 0; i<events.size(); i++){
       
        series.getData().add(new XYChart.Data(events.get(i).getxCoord(),events.get(i).getyCoord()));
      
        }
         
        sc.getData().addAll(series);
        sc.setPrefSize(500, 500);
        
        
        
        
        
        //adding field
        VBox lblBox = new VBox();
        VBox titleBox = new VBox();
        VBox buttons = new VBox(); 
        VBox input = new VBox();
        VBox whole = new VBox();
        HBox hbox = new HBox(10);
        
        Scene scene  = new Scene(whole);
        
        Label title = new Label("Enter your Location:");
        final TextField xField = new TextField ();
        Label xLabel = new Label("X Coordinate:");
        Label yLabel = new Label("Y Coordinate:"); 
        
        final TextField yField = new TextField ();
        xField.setPrefWidth(10);
        yField.setPrefWidth(10);
        Button enterBtn = new Button(); 
        Button removeLocation = new Button();
        enterBtn.setText("Enter");
        removeLocation.setText("Remove Previous Location");
        
        lblBox.setMinSize(100, 100);
        lblBox.setPadding(new Insets(10,10,10,10));
        lblBox.getChildren().addAll(xLabel,yLabel);
        
        input.getChildren().addAll(xField,yField);
        input.setPadding(new Insets(10,10,10,10));
        input.setMinSize(50, 50);
        
        buttons.getChildren().addAll(enterBtn,removeLocation);
        buttons.setMinSize(50, 50);
        buttons.setPadding(new Insets(10,10,10,10));
        
        hbox.getChildren().addAll(lblBox,input,buttons);
        hbox.setMinSize(50, 50);
        
        titleBox.getChildren().addAll(title,hbox);
        titleBox.setMinSize(50, 50);
        titleBox.setPadding(new Insets(10,10,10,10));
       
        whole.getChildren().addAll(sc, titleBox);
        
        
    removeLocation.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
        if (!sc.getData().isEmpty()&& sc.getData().size()>1) 
            sc.getData().remove((sc.getData().size()-1));
        }
    });
        
        enterBtn.setOnAction(new EventHandler<ActionEvent>() {
        
        @Override
        public void handle(ActionEvent event) {
        XYChart.Series seriesYou = new XYChart.Series();  
        ArrayList<Event> nearestEvent = new ArrayList<Event>();   
        String tempX= xField.getText();
        String tempY = yField.getText();
        
        Matcher mx = pattern.matcher(tempX);
        Matcher my = pattern.matcher(tempY);

        //using regex to santise the user input.
        if (mx.matches()&& my.matches()) { 
            final Integer tempIntX =Integer.parseInt(tempX);
            final Integer tempIntY =Integer.parseInt(tempY);
            if( tempIntX <= 10 && tempIntY<=10 && tempIntX >= -10 && tempIntY>=-10){
                seriesYou.setName("Your Location");
                seriesYou.getData().add(new XYChart.Data(tempIntX, tempIntY));
                sc.getData().addAll(seriesYou);
                nearestEvent = calculateDistance(events, tempIntX, tempIntY, sc);
                dialogEvents(nearestEvent, sc);
            }else{
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setHeaderText("Invalid Input!");
                alert.setContentText("Please enter an x and y coordinate pair from 10 to -10");

                alert.showAndWait();
            }
        }else{
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Invalid Input!");
            alert.setContentText("Please enter an x and y coordinate pair from 10 to -10");

            alert.showAndWait();
        }
          
             
           
        }
        });
       
        //Setting the stage for the program.
        primaryStage.setTitle("Viagogo Events!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
        Integer count=0;
        //setting the tooltips for each event.
        for (XYChart.Series<Number, Number> s : sc.getData()) {
            for (XYChart.Data<Number, Number> d : s.getData()) {
                
                Tooltip.install(d.getNode(), new Tooltip(
                    String.format("Event Code: "+events.get(count).getEventCode().toString()+"\n"+
                            "No of Tickets: "+events.get(count).getNumberOfTickets().toString()+"\n"+
                            "Price: " + "$"+events.get(count).getTicketPrice().toString())));
                        
            count++;
                
            }   
    
        }
    }


    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
